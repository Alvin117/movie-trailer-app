package com.example.movietrailerapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.denzcoskun.imageslider.ImageSlider
import com.denzcoskun.imageslider.models.SlideModel
import com.example.movietrailerapp.adapter.ApiAdapter
import com.example.movietrailerapp.model.MovieDaoModel
import com.example.movietrailerapp.model.MovieModel
import com.example.movietrailerapp.viewmodel.MovieListViewModel
import com.example.movietrailerapp.viewmodel.UserViewModel

class MainActivity : AppCompatActivity() {

    lateinit var viewModel: MovieListViewModel
    private lateinit var recyclerView: RecyclerView
    private lateinit var MoviesAdapter: ApiAdapter
    lateinit var loginViewModel: UserViewModel
    lateinit var movieViewModel: MovieListViewModel
    private val imageList = ArrayList<SlideModel>()


    override fun onCreate(savedInstanceState:Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //Instance the view model class
        loginViewModel = ViewModelProvider(this).get(UserViewModel::class.java)
        movieViewModel = ViewModelProvider(this).get(MovieListViewModel::class.java)
        viewModel = ViewModelProvider(this).get(MovieListViewModel::class.java)

        //Call observe models
        bindViewModel()
        viewModel.getMovies()

        //Validate if user logout
        logout()
    }

    //Validate if movie exists on database
    fun observeExistData(context: Context, id: Int, title: String, overview: String, poster_path: String, backdrop_path: String, release_date: String, category: String){
        movieViewModel.getMovieData(context, id)!!.observe(this, Observer {
            if (it == null) {
                movieViewModel.insertData(context, id, title, overview, poster_path, backdrop_path, release_date, category)
            }
        })
    }

    //User press logout
    private fun logout() {
        val btnLogout = findViewById<Button>(R.id.logoutButton)
        btnLogout.setOnClickListener {
            loginViewModel.deleteUser(this)
            Toast.makeText(this,"Has cerrado sesion", Toast.LENGTH_SHORT).show()
            val intent = Intent(this, LoginActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }
    }

    //Observe if get api response
    private fun bindViewModel() {

        //Observe popular movies
        viewModel.currentName.observe(this, Observer<MovieModel> {
            recyclerView = findViewById(R.id.recyclerView)
            recyclerView.layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            MoviesAdapter = ApiAdapter(it.results)
            recyclerView.adapter = MoviesAdapter
            observeExistData(this@MainActivity, it.results[0].id, it.results[0].title, it.results[0].overview, it.results[0].poster_path, it.results[0].backdrop_path, it.results[0].release_date, "Popular")
        })

        //Observe top rated movies
        viewModel.mutableTopRated.observe(this, Observer<MovieModel> {
            recyclerView = findViewById(R.id.recyclerView_qualified)
            recyclerView.layoutManager = LinearLayoutManager(
                this@MainActivity,
                LinearLayoutManager.HORIZONTAL,
                false
            )
            MoviesAdapter = ApiAdapter(it.results)
            recyclerView.adapter = MoviesAdapter
            observeExistData(this@MainActivity, it.results[0].id, it.results[0].title, it.results[0].overview, it.results[0].poster_path, it.results[0].backdrop_path, it.results[0].release_date, "TopRated")

        })

        //Observe upcoming movies
        viewModel.mutableUpcoming.observe(this, Observer<MovieModel> {
            for (i in 0..4){
                imageList.add(SlideModel(it.results.get(i).getBackgroudPath(),it.results.get(i).title))
            }
            val imageSlider = findViewById<ImageSlider>(R.id.image_slider)
            imageSlider.setImageList(imageList)
            observeExistData(this@MainActivity, it.results[0].id, it.results[0].title, it.results[0].overview, it.results[0].poster_path, it.results[0].backdrop_path, it.results[0].release_date, "Upcoming")

        })
    }

}