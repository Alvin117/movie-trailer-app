package com.example.movietrailerapp.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivities
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.movietrailerapp.MovieDetailActivity
import com.example.movietrailerapp.R
import com.example.movietrailerapp.model.MovieModel
import com.example.movietrailerapp.model.Result


class ApiAdapter(val movies: List<Result>): RecyclerView.Adapter<ApiAdapter.MoviesViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        return MoviesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return movies.size
    }

    override fun onBindViewHolder(holder: MoviesViewHolder, position: Int) {

        return holder.bind(movies.get(position))

    }

    inner class MoviesViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
        private val photo: ImageView = itemView.findViewById(R.id.movie_photo)
        private val title: TextView = itemView.findViewById(R.id.movie_title)
        private val overview:TextView = itemView.findViewById(R.id.movie_overview)
        private var movie: Result? = null

        fun bind(movie: Result) {
            Glide.with(itemView.context)
                .load(movie.getImagePath())
                .thumbnail(0.33f)
                .placeholder(R.drawable.ic_launcher_background)
                .centerCrop()
                .into(photo)
            title.text = movie.title
            overview.text = movie.overview
            itemView.setOnClickListener {
                val context = itemView.context
                val showIntent = Intent(context, MovieDetailActivity::class.java)
                showIntent.putExtra("IMAGE_KEY", movie.id)
                context.startActivity(showIntent)
            }
        }

    }
}



