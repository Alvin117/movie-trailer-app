package com.example.movietrailerapp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movietrailerapp.Repository.MovieRepository
import com.example.movietrailerapp.Repository.UserRepository
import com.example.movietrailerapp.api.ApiService
import com.example.movietrailerapp.api.ServiceBuilder
import com.example.movietrailerapp.model.MovieDaoModel
import com.example.movietrailerapp.model.MovieModel
import com.example.movietrailerapp.model.User
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieListViewModel : ViewModel() {

    val currentName: MutableLiveData<MovieModel> by lazy { MutableLiveData<MovieModel>() }
    val mutableTopRated: MutableLiveData<MovieModel> by lazy { MutableLiveData<MovieModel>() }
    val mutableUpcoming: MutableLiveData<MovieModel> by lazy { MutableLiveData<MovieModel>() }
    var liveDataMovies: LiveData<MovieDaoModel>? = null

    private val API_KEY = "17cd4871da26aeebb103e9cc5d48e0e5"

    val request = ServiceBuilder.buildService(ApiService::class.java)

    fun getMovies() {
        popularApiCall()
        nowPlayingApiCall()
        upcomingApiCall()
    }

    private fun popularApiCall(){
        val call = request.getMovies(API_KEY)
        call.enqueue(object : Callback<MovieModel> {
            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                if(response.isSuccessful){
                    currentName.postValue(response.body())

                }else{
                   currentName.postValue(null)
                }
            }
            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                currentName.postValue(null)
            }
        })
    }

    private fun nowPlayingApiCall(){
        val call = request.getNowPlaying(API_KEY)
        call.enqueue(object : Callback<MovieModel> {
            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                if(response.isSuccessful){
                    mutableTopRated.postValue(response.body())

                }else{
                    mutableTopRated.postValue(null)
                }
            }
            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                mutableTopRated.postValue(null)
            }
        })
    }

    private fun upcomingApiCall(){
        val call = request.getUpcoming(API_KEY)
        call.enqueue(object : Callback<MovieModel> {
            override fun onResponse(call: Call<MovieModel>, response: Response<MovieModel>) {
                if(response.isSuccessful){
                    mutableUpcoming.postValue(response.body())
                }else{
                    mutableUpcoming.postValue(null)
                }
            }
            override fun onFailure(call: Call<MovieModel>, t: Throwable) {
                mutableUpcoming.postValue(null)
            }
        })
    }


    fun insertData(context: Context, id: Int, title: String, overview: String, poster_path: String, backdrop_path: String, release_date: String, category: String) {
        MovieRepository.insertData(context, id, title, overview, poster_path, backdrop_path, release_date, category)
    }

    fun getMovieData(context: Context, id: Int) : LiveData<MovieDaoModel>? {
        liveDataMovies = MovieRepository.getMoviesData(context,id)
        return liveDataMovies
    }

}