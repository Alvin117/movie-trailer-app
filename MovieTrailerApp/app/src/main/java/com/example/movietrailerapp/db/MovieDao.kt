package com.example.movietrailerapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movietrailerapp.model.MovieDaoModel
import com.example.movietrailerapp.model.User

@Dao
interface MovieDao {

    //Insert movie
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(newMovie: MovieDaoModel)

    //Get movie validate
    @Query("SELECT * FROM MovieDaoModel WHERE id = :id")
    fun findMovie(id: Int) : LiveData<MovieDaoModel>

}