package com.example.movietrailerapp.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.movietrailerapp.model.User

@Dao
interface UserDao {

    //Insert user
    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insert(newUser: User)

    //Delete user
    @Query("DELETE FROM User")
    fun deleteAll()

    //Get user validate
    @Query("SELECT * FROM User WHERE user_col = :user AND password_col =:password")
    fun findUser(user: String, password: String) : LiveData<User>

    //Get number of rows
    @Query("SELECT COUNT(*) FROM User")
    fun getCount(): LiveData<Int>

}