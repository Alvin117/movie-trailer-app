package com.example.movietrailerapp.Repository

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import com.example.movietrailerapp.db.UserDatabase
import com.example.movietrailerapp.model.MovieDaoModel
import com.example.movietrailerapp.model.User

class MovieRepository (application: Application){
    companion object {

        var movieDatabase: UserDatabase? = null
        var movieTableModel: LiveData<MovieDaoModel>? = null


        fun initializeDB(context: Context) : UserDatabase {
            return UserDatabase.getInstance(context)
        }

        fun insertData(context: Context,id: Int, title: String, overview: String, poster_path: String, backdrop_path: String, release_date: String, category: String) {
            movieDatabase = initializeDB(context)
            val loginDetails = MovieDaoModel(id, title, overview, poster_path, backdrop_path, release_date, category)
            movieDatabase!!.movieDao().insert(loginDetails)
        }

        fun getMoviesData(context: Context, id: Int) : LiveData<MovieDaoModel>? {
            movieDatabase = initializeDB(context)
            movieTableModel = movieDatabase!!.movieDao().findMovie(id)
            return movieTableModel
        }

    }
}