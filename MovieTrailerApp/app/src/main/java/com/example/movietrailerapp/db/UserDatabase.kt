package com.example.movietrailerapp.db

import android.app.Application
import android.content.Context
import android.os.AsyncTask
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.movietrailerapp.model.MovieDaoModel
import com.example.movietrailerapp.model.MovieDetailModel
import com.example.movietrailerapp.model.User

@Database(entities = [User::class, MovieDaoModel::class], version = 1)
abstract class UserDatabase : RoomDatabase() {

    abstract fun userDao() : UserDao
    abstract fun movieDao(): MovieDao

    //Create the database
    companion object {
        private val lock = Any()
        private const val DB_NAME = "AppMovie.db"
        private var INSTANCE: UserDatabase? = null

        //Instance database
        fun getInstance(context: Context): UserDatabase {
            synchronized(lock) {
                if (INSTANCE == null) {
                    INSTANCE =
                            Room.databaseBuilder(context,
                                    UserDatabase::class.java, DB_NAME)
                                    .allowMainThreadQueries()
                                    .build()
                }
                return INSTANCE!!
            }
        }
    }

}