package com.example.movietrailerapp.model
import androidx.room.Entity


data class MovieDetailModel(
        val id: Int,
        val overview: String,
        val backdrop_path: String,
        val title: String
){

    fun getBackgroudPathDetail():String{
        return "https://image.tmdb.org/t/p/w500/$backdrop_path"
    }
}