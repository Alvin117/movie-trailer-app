package com.example.movietrailerapp.api

import com.example.movietrailerapp.model.MovieDetailModel
import com.example.movietrailerapp.model.MovieModel
import com.example.movietrailerapp.model.MovieTrailerModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {

    //Declare api requests
    @GET("3/movie/popular")
    fun getMovies(@Query("api_key") key: String): Call<MovieModel>

    @GET("3/movie/top_rated")
    fun getNowPlaying(@Query("api_key") key: String): Call<MovieModel>

    @GET("3/movie/upcoming")
    fun getUpcoming(@Query("api_key") key: String): Call<MovieModel>

    @GET("3/movie/{id}")
    fun getMovieDetail(@Path("id") id: Int, @Query("api_key") key: String): Call<MovieDetailModel>

    @GET("3/movie/{id}/videos")
    fun getTrailer(@Path("id") id: Int, @Query("api_key") key: String): Call<MovieTrailerModel>

}