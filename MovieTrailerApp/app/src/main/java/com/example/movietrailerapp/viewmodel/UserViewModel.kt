package com.example.movietrailerapp.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.example.movietrailerapp.Repository.UserRepository
import com.example.movietrailerapp.model.User

class UserViewModel : ViewModel() {
    var liveDataLogin: LiveData<User>? = null
    var liveDataInt: LiveData<Int>? = null

    fun insertData(context: Context, username: String, password: String) {
        UserRepository.insertData(context, username, password)
    }

    fun getLoginDetails(context: Context, username: String, password: String) : LiveData<User>? {
        liveDataLogin = UserRepository.getLoginDetails(context, username, password)
        return liveDataLogin
    }

    fun deleteUser(context: Context){
        UserRepository.deleteData(context)
    }

    fun getSize(context: Context) : LiveData<Int>? {
        liveDataInt = UserRepository.databaseSize(context)
        return liveDataInt

    }
}