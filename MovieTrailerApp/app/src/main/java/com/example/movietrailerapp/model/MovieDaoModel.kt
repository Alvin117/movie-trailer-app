package com.example.movietrailerapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class MovieDaoModel (
        @PrimaryKey(autoGenerate = false) var id: Int = 0,
        var title: String = "",
        var overview: String = "",
        var poster_path: String = "",
        var backdrop_path: String = "",
        var release_date: String = "",
        var category: String = ""
)