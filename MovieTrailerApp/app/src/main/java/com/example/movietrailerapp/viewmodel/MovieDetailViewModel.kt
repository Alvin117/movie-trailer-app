package com.example.movietrailerapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.movietrailerapp.api.ApiService
import com.example.movietrailerapp.api.ServiceBuilder
import com.example.movietrailerapp.model.MovieDetailModel
import com.example.movietrailerapp.model.MovieModel
import com.example.movietrailerapp.model.MovieTrailerModel

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MovieDetailViewModel : ViewModel() {

    val mutableDetailMovie: MutableLiveData<MovieDetailModel> by lazy { MutableLiveData<MovieDetailModel>() }
    val mutableTrailerMovie: MutableLiveData<MovieTrailerModel> by lazy { MutableLiveData<MovieTrailerModel>() }
    private val API_KEY = "17cd4871da26aeebb103e9cc5d48e0e5"
    val request = ServiceBuilder.buildService(ApiService::class.java)


    fun detailMovieCall(idMovie: Int){
        val call = request.getMovieDetail(idMovie, API_KEY)
        call.enqueue(object : Callback<MovieDetailModel> {
            override fun onResponse(call: Call<MovieDetailModel>, response: Response<MovieDetailModel>) {
                if(response.isSuccessful){
                    mutableDetailMovie.postValue(response.body())
                }else{
                    mutableDetailMovie.postValue(null)
                }
            }
            override fun onFailure(call: Call<MovieDetailModel>, t: Throwable) {
                mutableDetailMovie.postValue(null)
            }
        })
    }

    fun trailerMovieCall(idMovie: Int){
        val call = request.getTrailer(idMovie, API_KEY)
        call.enqueue(object : Callback<MovieTrailerModel> {
            override fun onResponse(call: Call<MovieTrailerModel>, response: Response<MovieTrailerModel>) {
                if(response.isSuccessful){
                    mutableTrailerMovie.postValue(response.body())
                }else{
                    mutableTrailerMovie.postValue(null)
                }
            }
            override fun onFailure(call: Call<MovieTrailerModel>, t: Throwable) {
                mutableTrailerMovie.postValue(null)
            }
        })
    }



}