package com.example.movietrailerapp.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class User (
     var user_col: String = "",
     var password_col: String = "",
     @PrimaryKey(autoGenerate = true) var id: Int = 0


)