package com.example.movietrailerapp

import android.annotation.SuppressLint
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.annotation.RequiresApi

class MovieTrailerActivity : AppCompatActivity() {
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_trailer)

        //Receive the movietrailer url
        val movieUrl = intent.getStringExtra("URL")
        webViewSetup(movieUrl)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("SetJavaScriptEnabled")

    //Load the url on the web view
    private fun webViewSetup(movieUrl: String?) {
        val webView = findViewById<WebView>(R.id.videoWebView)
        webView.webViewClient = WebViewClient()
        webView.apply {
            loadUrl("https://www.youtube.com/watch?v=$movieUrl")
            settings.javaScriptEnabled = true
            settings.safeBrowsingEnabled = true
        }
    }


}