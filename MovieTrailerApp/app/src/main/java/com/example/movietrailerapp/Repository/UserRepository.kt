package com.example.movietrailerapp.Repository

import android.app.Application
import android.content.Context
import androidx.lifecycle.LiveData
import com.example.movietrailerapp.db.UserDatabase
import com.example.movietrailerapp.model.User

class UserRepository (application: Application) {

    companion object {

        var loginDatabase: UserDatabase? = null

        var loginTableModel: LiveData<User>? = null


        fun initializeDB(context: Context) : UserDatabase {
            return UserDatabase.getInstance(context)
        }

        fun insertData(context: Context, username: String, password: String) {
            loginDatabase = initializeDB(context)
            val loginDetails = User(username, password)
            loginDatabase!!.userDao().insert(loginDetails)
        }

        fun getLoginDetails(context: Context, username: String, password: String) : LiveData<User>? {
            loginDatabase = initializeDB(context)
            loginTableModel = loginDatabase!!.userDao().findUser(username, password)
            return loginTableModel
        }

        fun deleteData(context: Context){
            loginDatabase = initializeDB(context)
            loginDatabase!!.userDao().deleteAll()

        }

        fun databaseSize(context: Context) : LiveData<Int>{
            loginDatabase = initializeDB(context)
            return loginDatabase!!.userDao().getCount()
        }

    }


}