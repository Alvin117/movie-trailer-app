package com.example.movietrailerapp

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.movietrailerapp.viewmodel.UserViewModel

class LoginActivity : AppCompatActivity() {

    lateinit var loginViewModel: UserViewModel
    lateinit var context: Context
    lateinit var strUsername: String
    lateinit var strPassword: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val btnReadLogin = findViewById<Button>(R.id.loginButton)
        val userEditText = findViewById<EditText>(R.id.userEditText)
        val passwordEditText = findViewById<EditText>(R.id.passwordEditText)
        context = this@LoginActivity
        loginViewModel = ViewModelProvider(this).get(UserViewModel::class.java)

        //Validate if exists user in the database
        if (observeSize() > 0){
            launchMain()
        } else {
            btnReadLogin.setOnClickListener {
                strUsername = userEditText.text.toString().trim()
                strPassword = passwordEditText.text.toString().trim()
                validateData(strUsername, strPassword)
            }
        }

    }

    //Validate if user types data
    private fun validateData(strUsername: String, strPassword: String) {
        if (strUsername.isEmpty()) {
            Toast.makeText(context,"Ingrese un usuario", Toast.LENGTH_SHORT).show()
        } else if (strPassword.isEmpty()) {
            Toast.makeText(context,"Ingrese una contraseña",Toast.LENGTH_SHORT).show()
        } else {
            if (strUsername.equals("Usuario@gmail.com") && strPassword.equals("1234")){
                observeExistData()
                observeSize()

            } else {
                Toast.makeText(context,"Acceso invalido",Toast.LENGTH_SHORT).show()
            }
        }
    }

    //Get the database number of rows
    fun observeSize() : Int{
        var size = 0
        loginViewModel.getSize(context)!!.observe(this, Observer {
            if (it == null) {
                Toast.makeText(context,"No hay datos",Toast.LENGTH_SHORT).show()
            } else {
                size = it

            }
        })
        return size
    }

    //Validate if user exists on database
    fun observeExistData(){
        loginViewModel.getLoginDetails(context, strUsername, strPassword)!!.observe(this, Observer {
            if (it == null) {
                loginViewModel.insertData(context, strUsername, strPassword)
            }
        })
        launchMain()
    }

    //Call main activity
    fun launchMain(){
        Toast.makeText(context,"Bienvenido",Toast.LENGTH_SHORT).show()
        val intent = Intent(this, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        startActivity(intent)
    }
}