package com.example.movietrailerapp.model

data class MovieTrailerModel(
    val results: List<TrailerResult>
)

data class TrailerResult(
    val id: String,
    val key: String,
    val name: String
)