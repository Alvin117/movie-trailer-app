package com.example.movietrailerapp.model

data class MovieModel(
    val results: List<Result>
)

data class Result(
    val id: Int,
    val overview: String,
    val poster_path: String,
    val backdrop_path: String,
    val release_date: String,
    val title: String,
    val vote_average: Double,
    val vote_count: Int
){
    fun getImagePath() : String {
        return "https://image.tmdb.org/t/p/w342/$poster_path"
    }

    fun getBackgroudPath():String{
        return "https://image.tmdb.org/t/p/w500/$backdrop_path"
    }
}