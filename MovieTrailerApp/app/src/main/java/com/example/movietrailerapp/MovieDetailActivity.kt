package com.example.movietrailerapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bumptech.glide.Glide
import com.example.movietrailerapp.viewmodel.MovieDetailViewModel

class MovieDetailActivity : AppCompatActivity() {

    lateinit var viewModel: MovieDetailViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        val movieId = intent.getIntExtra("IMAGE_KEY",0)

        //Instance view model class
        viewModel = ViewModelProvider(this).get(MovieDetailViewModel::class.java)
        bindDetailViewModel()
        viewModel.detailMovieCall(movieId)
        viewModel.trailerMovieCall(movieId)

    }



    private fun bindDetailViewModel() {
        val photo = findViewById<ImageView>(R.id.detail_bannerImage)
        val title = findViewById<TextView>(R.id.detail_title)
        val overview = findViewById<TextView>(R.id.detail_overview)
        val button = findViewById<Button>(R.id.trailerButton)

        //observe detail movies
        viewModel.mutableDetailMovie.observe(this, Observer {
            Glide.with(this)
                .load(it.getBackgroudPathDetail())
                .thumbnail(0.33f)
                .placeholder(R.drawable.ic_launcher_background)
                .centerCrop()
                .into(photo)
            title.setText(it.title)
            overview.setText(it.overview)
        })

        //observe trailer movies
        viewModel.mutableTrailerMovie.observe(this, Observer { result ->
            //Call trailer activiy
            button.setOnClickListener{
                var videoId = result.results
                if(videoId.isNullOrEmpty()){
                    Toast.makeText(this,"No hay trailer disponible", Toast.LENGTH_SHORT).show()
                }else{
                    val intent = Intent(this, MovieTrailerActivity::class.java)
                    intent.putExtra("URL", result.results.get(0).key)
                    startActivity(intent)
                }
            }
        })

    }
}